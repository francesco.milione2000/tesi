use actix_web::{
    get,
    web::{self, Data},
    App,
    HttpResponse, 
    HttpServer, 
    Responder,
    middleware::Logger
};


use std::{
    env,
    thread,
    time::SystemTime,
    process::Command,
    time::UNIX_EPOCH,
};

use env_logger;
use paho_mqtt as mqtt;
use mqtt::AsyncClient;

fn print(tempo: u128){
    let the_command = format!("echo {:?} >> delta.csv", tempo);
    Command::new("/bin/bash")
        .arg("-c")
        .arg(the_command.clone())
        .output()
        .expect(":: Failed to run the command ::");

}

#[get("/http/{topic}/{message}")]
async fn trigger(path: web::Path<(String, String)>, client: Data<AsyncClient>) -> impl Responder {

    let (topic, message) = path.into_inner();

    //Tempo Iniziale
    let tempo_iniziale = SystemTime::now()
        .duration_since(UNIX_EPOCH)
        .unwrap()
        .as_micros();

    let tempo_string = format!("{}", tempo_iniziale);
    
    let message = format!("{}#{}", message.clone(), tempo_string);

    let msg = mqtt::Message::new(topic.clone(), message.clone(), 0);
    
    client.publish(msg);
   
    HttpResponse::Ok().body("Ok")
    
}

fn thread_risposte(client: mqtt::Client){
    let rx = client.start_consuming();
    client.subscribe("output", 0).unwrap();

    for msg in rx.iter(){
        match msg {
            Some(m) => {
                //println!("{}", m);
                let messaggio = m.to_string();
                let mut split = messaggio.split('#');
                split.next();
                
                let tempo_finale = SystemTime::now()
                    .duration_since(UNIX_EPOCH)
                    .unwrap()
                    .as_micros();

                let tempo_iniziale:u128 = split.next().unwrap().parse().unwrap();

                let difference = (tempo_finale - tempo_iniziale)/1000;
                
                //println!("{}", difference);
                print(difference);
            }
            None => println!("Errore")
        }
    }
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    env_logger::init();

    let triggerport = option_env!("TRIGGERPORT").or(Some("8081")).unwrap();
    let port = triggerport.parse::<u16>().unwrap_or(8080);
    let server = env::var("SERVER").unwrap_or("127.0.0.1".to_string());
    let port_c = env::var("PORT").unwrap_or("1883".to_string());
    let indirizzo = format!("tcp://{}:{}", server, port_c);
    
    let cli = AsyncClient::new(indirizzo.clone()).unwrap();
    let conn_opts = mqtt::ConnectOptionsBuilder::new().finalize();
    cli.connect(conn_opts.clone());

    let cli_s = mqtt::Client::new(indirizzo.clone()).unwrap();
    cli_s.connect(conn_opts.clone()).unwrap();

    let r = thread::Builder::new().name("Output".to_string()).spawn(move || {
        thread_risposte(cli_s);
    });
    
    HttpServer::new(move || {
        App::new()
            .data(cli.clone())
            .wrap(Logger::default())
            .wrap(Logger::new("%a %{User-Agent}i"))
            .service(trigger)
    })
    .bind(("0.0.0.0",port))?
    .run()
    .await

}
