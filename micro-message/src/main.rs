use std::{
    process,
    env,
    process::Command
};

extern crate paho_mqtt as mqtt;

use std::time::SystemTime;

fn print(tempo: u128){
    let the_command = format!("echo {:?} >> delta.csv", tempo);
    Command::new("/bin/bash")
        .arg("-c")
        .arg(the_command.clone())
        .output()
        .expect(":: Failed to run the command ::");

}


fn main() {

    let server = env::var("SERVER").unwrap_or("127.0.0.1".to_string());
    let port = env::var("PORT").unwrap_or("1883".to_string());
    let topic = env::var("TOPIC").unwrap_or("topic".to_string());
    let trigger = env::var("TRIGGER").unwrap_or("trigger".to_string());

    let indirizzo = format!("tcp://{}:{}", server, port);
    
    println!("Mi connetto a {}", indirizzo.clone());

    // Creo cliente specificando ip e porta, esco in caso di errore
    let mut cli = mqtt::Client::new(indirizzo.clone()).unwrap_or_else(|err| {
        println!("Errore creazione cliente: {:?}", err);
        process::exit(1);
    });

    // Inizializzo il consumer
    let rx = cli.start_consuming();

    // Creo le opzioni di connessione, volendo potevo mettere qui ip e porta
    let conn_opts = mqtt::ConnectOptions::new();

    // Mi connetto, in caso di errore esco
    if let Err(e) = cli.connect(conn_opts) {
        println!("Errore connessione:{:?}", e);
        process::exit(1);
    }

    // Sono connesso
    //println!("Topic registrato: [{}]\n", topic);

    if let Err(e) = cli.subscribe(&topic, 0 as i32) {
        println!("Errore iscrizione al topic: {:?}", e);
        process::exit(1);
    }

    //println!("Attendo messaggi...");
    for msg in rx.iter() {
        let tempo_iniziale = SystemTime::now();
       
        if let Some(msg) = msg {
            

            let create_opts = mqtt::CreateOptionsBuilder::new()
                .server_uri(indirizzo.clone())
                .client_id("MQTT trigger")
                .finalize();


            let cli_p = mqtt::AsyncClient::new(create_opts).unwrap_or_else(|err| {
                println!("Errore creazione Client sub: {}", err);
                process::exit(1);
            });

            let conn_opts = mqtt::ConnectOptionsBuilder::new().finalize();

            //Mi connetto
            if let Err(e) = cli_p.connect(conn_opts).wait() {
                println!("Errore connessione:{:?}", e);
            }
            
            let m = format!("{}", msg);
            let messaggio = mqtt::Message::new(trigger.clone(), m.clone(), 0);

            cli_p.publish(messaggio);

            println!("Invio {} {}", topic.clone(), m.clone());

            cli_p.disconnect(None);
            

        }
        else if !cli.is_connected() {
            break;
        }

        let tempo_finale = SystemTime::now();
        let difference = tempo_finale
            .duration_since(tempo_iniziale)
            .expect("Clock may have gone backwards");
        
        print(difference.as_millis());
    }

    // Mi disconnetto
    if cli.is_connected() {
        println!("Disconnessione");
        cli.unsubscribe(&topic).unwrap();
        cli.disconnect(None).unwrap();
    }
    println!("Fine");
}